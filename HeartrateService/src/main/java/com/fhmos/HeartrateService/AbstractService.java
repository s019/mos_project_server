package com.fhmos.HeartrateService;


import io.reactivex.Flowable;

public class AbstractService<T extends AbstractRepository> {

    protected T repository;

    public AbstractService(T repository) {
        this.repository = repository;
    }

    public Flowable<ResponseID> getIdFrom(String table) {
        return this.repository.getIdFrom(table);
    }
}
