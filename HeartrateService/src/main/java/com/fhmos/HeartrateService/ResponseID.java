package com.fhmos.HeartrateService;


import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseID {

    @JsonProperty
    private String id;

    public ResponseID(String id) {
        this.id = id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }
}
