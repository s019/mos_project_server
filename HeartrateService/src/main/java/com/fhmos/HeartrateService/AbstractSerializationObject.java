package com.fhmos.HeartrateService;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AbstractSerializationObject<T> {

    @JsonProperty("content")
    protected T rawContent;

    public AbstractSerializationObject(T rawContent) {
        this.rawContent = rawContent;
    }

    public AbstractSerializationObject() {}

    public T getRawContent() {
        return this.rawContent;
    }

    public void setRawContent(T rawContent) {
        this.rawContent = rawContent;
    }
}
