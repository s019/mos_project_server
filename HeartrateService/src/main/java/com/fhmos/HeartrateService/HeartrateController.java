package com.fhmos.HeartrateService;


import io.reactivex.Flowable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(path = "/heartrates", produces = { APPLICATION_JSON_UTF8_VALUE })
public class HeartrateController extends AbstractController<HeartrateService> {

    public HeartrateController(HeartrateService service) {
        super(service, "heartrate");
    }

    @PostMapping(path = "/{id}", produces = { APPLICATION_JSON_UTF8_VALUE }, consumes = { APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Flowable<ResponseID>> updateHeartrate(@PathVariable String id, @RequestBody Flowable<Heartrate> heartrate) {

     /*   System.out.println("1 " + heartrate.getValue());
        System.out.println("2 " + heartrate.getId());
        System.out.println("3 " + heartrate.getSessionID());
        System.out.println("4 " + heartrate.getTimestamp());
       return new ResponseEntity<Flowable<ResponseID>>(Flowable.just(new ResponseID("asldköfjaskl")), HttpStatus.ACCEPTED);*/
        return new ResponseEntity<Flowable<ResponseID>>(this.service.updateHeartrate(id, heartrate).map(id_ -> new ResponseID(id_)), HttpStatus.ACCEPTED);
    }
}
