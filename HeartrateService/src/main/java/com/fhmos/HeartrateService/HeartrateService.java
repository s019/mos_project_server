package com.fhmos.HeartrateService;

import org.springframework.stereotype.Service;

import io.reactivex.Flowable;
import org.springframework.stereotype.Service;

@Service
public class HeartrateService extends AbstractService<HeartrateRepository> implements IHeartrateHandler {

    public HeartrateService(HeartrateRepository repository) {
        super(repository);
    }

    public Flowable<String> updateHeartrate(String id, Flowable<Heartrate> heartrate) {
        return this.repository.updateHeartrate(id, heartrate);
    }
}
