package com.fhmos.HeartrateService;

import io.reactivex.Flowable;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Repository
public class HeartrateRepository extends AbstractRepository {

    public HeartrateRepository() throws SQLException, ClassNotFoundException {
        super();
    }

    public Flowable<String> updateHeartrate(String id, Flowable<Heartrate> heartrate) {
        //Flowable<String> replayId = id.replay().autoConnect();
        Flowable<Heartrate> rate = heartrate.replay().autoConnect();

        Flowable<Integer> update1 = this
                .database
                .update("update heartrate set timestamp = ? where id = \""+ id +"\";")
                .parameterStream(
                        rate.map(hrate -> {
                            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(hrate.getTimestamp());
                            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
                        }))
                .counts();

        Flowable<Integer> update2 = this
                .database
                .update("update heartrate set value = ? where id = \""+ id +"\";")
                .parameterStream(rate.map(hrate -> hrate.getValue()))
                .dependsOn(update1)
                .counts();

        Flowable<Integer> update3 = this
                .database
                .update("update heartrate set session_id = ? where id = \""+ id +"\";")
                .parameterStream(rate.map(hrate -> hrate.getSessionID()))
                .dependsOn(update2)
                .counts();

        return this
                .database
                .select("select id from heartrate where id = \"" + id + "\";")
                .dependsOn(update3)
                .getAs(String.class);
    }
}
