package com.fhmos.HeartrateService;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Heartrate {

    @JsonProperty
    private String id;

    @JsonProperty
    private String timestamp;

    @JsonProperty("sessionID")
    private String sessionID;

    @JsonProperty
    private Double value;

    public Heartrate() { }

    public Heartrate(String id, String timestamp, String sessionID, Double value) {
        this.id = id;
        this.timestamp = timestamp;
        this.sessionID = sessionID;
        this.value = value;
    }


    public Double getValue() {
        return this.value;
    }

    public String getSessionID() {
        return this.sessionID;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public String getId() {
        return this.id;
    }
}
