//
//  ViewController.swift
//  test
//
//  Created by Manuel Leibetseder on 18.11.17.
//  Copyright © 2017 FH. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 60*60)
        print(date)
        let parameters: Parameters = [
            "content": dateFormatter.string(from: date)
        ]
        
        let header: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        print(parameters)
        Alamofire.request("http://192.168.1.46:8080/steps/date", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseString { response in
            print(response)
            let string = dateFormatter.string(from: date).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            print(string)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

