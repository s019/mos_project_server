import Objects.PostDate;
import Objects.ResponseID;
import Objects.Step;
import Service.StepService;
import com.fhmos.StepService.*;
import io.reactivex.Flowable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(path = "/steps", produces = { APPLICATION_JSON_UTF8_VALUE })
public class StepController {

    private StepService stepService;

    public StepController(StepService stepService) {
        this.stepService = stepService;
    }

    @CrossOrigin
    @RequestMapping(method = GET, produces = { APPLICATION_JSON_UTF8_VALUE })
    public Flowable<Step> getSteps() {

        System.out.println("Requested steps at: " + new Date());
        return this.stepService.getSteps();
    }

    @RequestMapping(path = "/date", method = POST, consumes = { APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Flowable<ResponseID>> createIdFor(@RequestBody Flowable<PostDate> postDate) {

        System.out.println("Retreiving date for step at: " + new Date());
        return new ResponseEntity<Flowable<ResponseID>>(this.stepService.createIdFor(
                postDate.map(content -> content.getRawContent())),
                HttpStatus.CREATED
        );
    }

   /* @RequestMapping(path = "/{stepID}/location", method = POST, consumes = { APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Flowable<String>>createLocationFor(@RequestBody Flowable<Objects.Location> postLocation, @PathVariable(value="stepID") String stepID) {

        System.out.println("Retreiving location for step id");
        return new ResponseEntity<Flowable<String>>(
                this.stepService.createLocationFor(postLocation, stepID),
                HttpStatus.CREATED
        );
    }

    @RequestMapping(path = "/{sessionID}", method = POST, consumes = { APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Flowable<String>>createIdFor(@RequestBody Flowable<Objects.PostDate> postDate, @PathVariable(value="sessionID") String sessionID) {

        System.out.println("Retreiving step for session id");
        return new ResponseEntity<Flowable<String>>(
                this.stepService.createIdFor(
                        postDate.map(content -> content.getRawContent()),
                        sessionID
                ),
                HttpStatus.CREATED
        );
    }*/
}
