package com.fhmos.StepService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@SpringBootApplication
@EnableDiscoveryClient
public class StepServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StepServiceApplication.class, args);
	}
}
