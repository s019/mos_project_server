package Abstract;

import Objects.ResponseID;
import io.reactivex.Flowable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

public class AbstractController<T extends AbstractService> {

    protected final T service;
    private final String table;

    public AbstractController(T service, String classType) {
        this.service = service;
        this.table   = classType;
    }

    @RequestMapping(path = "/id", produces = { APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Flowable<ResponseID>> getId() {
        return new ResponseEntity<>(
                this.service.getIdFrom(this.table),
                HttpStatus.CREATED
        );
    }
}
