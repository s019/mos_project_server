package Abstract;

import Objects.ResponseID;
import io.reactivex.Flowable;

public interface IAbstractHandler { //<T> {

    Flowable<ResponseID> getIdFrom(String table);
    //Flowable<T> getAll();
}
