package Abstract;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AbstractObject {

    @JsonProperty("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AbstractObject(String id) {
        this.id = id;
    }

    public AbstractObject() {}
}
