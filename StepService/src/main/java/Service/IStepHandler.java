package Service;

import Objects.Location;
import Objects.ResponseID;
import Objects.Step;
import com.fhmos.*;
import io.reactivex.Flowable;

public interface IStepHandler {

    Flowable<Step> getSteps();
    Flowable<ResponseID> createIdFor(Flowable<String> content);
    Flowable<String> createLocationFor(Flowable<Location> location, String stepID);
    Flowable<ResponseID> createIdFor(Flowable<String> content, Flowable<String> sessionID);
}
