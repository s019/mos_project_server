package Service;

import Objects.Location;
import Objects.ResponseID;
import Objects.Step;
import com.fhmos.*;
import io.reactivex.Flowable;
import org.springframework.stereotype.Service;

@Service
public class StepService implements IStepHandler {

    private StepRepository repository;

    public StepService(StepRepository repository) {
        this.repository = repository;
    }

    @Override
    public Flowable<Step> getSteps() {
        return this.repository.getSteps();
    }


    @Override
    public Flowable<ResponseID> createIdFor(Flowable<String> content) {
        return this.repository.createStepFrom(content);
    }

    @Override
    public Flowable<String> createLocationFor(Flowable<Location> location, String stepID) {
        return this.repository.createLocationFrom(location, stepID);
    }

    @Override
    public Flowable<ResponseID> createIdFor(Flowable<String> content, Flowable<String> sessionID) {
        return this.repository.createIdFor(content, sessionID);
    }

}

