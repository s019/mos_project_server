package Service;

import Abstract.AbstractRepository;
import Objects.Location;
import Objects.ResponseID;
import Objects.Step;
import com.fhmos.*;
import io.reactivex.Flowable;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Repository
public class StepRepository extends AbstractRepository {

    public StepRepository() throws SQLException, ClassNotFoundException {
        super();
    }

    public Flowable<Step> getSteps() {
       /* this
          .database
          .select("select id, date from steps").getAs(String.class, Date.class).blockingSubscribe(tuple -> {
            System.out.println(tuple._1());
            System.out.println(tuple._2());
        });*/

        return this
                .database
                .select("select id, date from steps")
                .getAs(String.class, Date.class)
                .map(sqlResult -> new Step(sqlResult.value1(), sqlResult.value2()));
    }

    public Flowable<ResponseID> createStepFrom(Flowable<String> date) {

       Flowable<Date> replayDate = date
               .map(content -> new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                       .parse(content)).replay().autoConnect();

        try {

            Flowable<Integer> update = this.database
                    .update("insert into steps(date) values(?)")
                    .parameterStream(replayDate).counts();

            return this.database
                    .select("select id from steps where date = ?")
                    .parameterStream(replayDate
                            .map(replayContent -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(replayContent))
                    ).dependsOn(update)
                    .getAs(String.class).map(string -> new ResponseID(string));

        } catch (Exception exc) {

            System.out.println(exc.getMessage());
            return Flowable.just(new ResponseID("-1"));
        }
    }

    public Flowable<String> createLocationFrom(Flowable<Location> postLocation, String stepID) {

        try {

            Flowable<Integer> update = this.database
                    .update("insert into location(x, y, step_id) values(?, ?, ?)")
                    .parameter("x", postLocation.map(location -> location.getX()))
                    .parameter("y", postLocation.map(location -> location.getY()))
                    .parameter("step_id", stepID).counts();

            return this.database
                    .select("select id from location where step_id = ?")
                    .parameter(stepID)
                    .dependsOn(update)
                    .getAs(String.class);

        } catch (Exception exc) {
            System.out.println(exc.getMessage());
            return Flowable.just("-1");
        }
    }

    public Flowable<ResponseID> createIdFor(Flowable<String> date, Flowable<String> sessionID) {

        Flowable<Date> replayDate = date
                .map(content -> new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                        .parse(content)).replay().autoConnect();

        try {

            Flowable<Integer> update = this.database
                    .update("insert into steps(date, session_id) values(?, ?)")
                    .parameterStream(replayDate)
                    .parameterStream(sessionID).counts();

            return this.database
                    .select("select id from steps where date = ?")
                    .parameterStream(replayDate
                            .map(replayContent -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(replayContent))
                    ).dependsOn(update)
                    .getAs(String.class).map(string -> new ResponseID(string));

        } catch (Exception exc) {

            System.out.println(exc.getMessage());
            return Flowable.just(new ResponseID("-1"));
        }
    }
}
