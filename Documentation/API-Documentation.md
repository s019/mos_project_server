# API

## Requests

### **GET** - /steps

#### Description
Fetching all steps from the Backend as Step-Objects.


#### CURL

```sh
curl -X GET "http://localhost:8080/steps" \
    -H "Content-Type: application/json"
```

#### Header Parameters

- **Content-Type** should respect the following schema:

```
{
  "type": "string",
  "enum": [
    "application/json"
  ],
  "default": "application/json"
}
```

### **POST** - /steps/date

#### Description
Posting the date as ISO8601-timestamp to save it as a step-object. The request response with a step-id if everything was fine. Send the date json formatted with the id "content" and value the date as "yyyy-MM-dd'T'HH:mm:ssZ".

#### CURL

```sh
curl -X POST "http://10.29.16.214:8080/steps/date" \
    -H "Content-Type: application/json" \
    --data-raw "$body"
```

#### Header Parameters

- **Content-Type** should respect the following schema:

```
{
  "type": "string",
  "enum": [
    "application/json"
  ],
  "default": "application/json"
}
```

#### Body Parameters

- **body** should respect the following schema:

```
{
  "type": "string",
  "default": "{\n  \"content\": \"2017-11-19T19:07:16+0100\"\n}"
}
```

## References

