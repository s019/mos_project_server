## Server
Enhanced to Microservices.

- [Management-Service](Management): Manages the Discovery (Eureka) and the Load-Balancing (Ribbon). Also Zuul works as a Gateway and redirects requests to the corresponding Service.
- [Step-Service](StepService): Steps are saved and read. 
- [Heartrate-Service](HeartrateService): Heartrate can be read and wrote.
- [Session-Service](SessionService): Sessions are created and read.

API-Documentation, like you can do it for every Service, is [here](Documentation/DocAPI-Documentation.md).

[SQL-Statements](sqldump.sql) were exported and can be restored to any MySQL-DB.