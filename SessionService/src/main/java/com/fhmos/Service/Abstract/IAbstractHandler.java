package com.fhmos.Service.Abstract;

import com.fhmos.Service.ResponseID;
import io.reactivex.Flowable;

public interface IAbstractHandler { //<T> {

    Flowable<ResponseID> getIdFrom(String table);
    //Flowable<T> getAll();
}
