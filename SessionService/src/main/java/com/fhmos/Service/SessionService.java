package com.fhmos.Service;

import com.fhmos.Service.Abstract.AbstractService;
import io.reactivex.Flowable;
import org.springframework.stereotype.Service;

@Service
public class SessionService extends AbstractService<SessionRepository> implements ISessionHandler {

    public SessionService(SessionRepository repository) {
        super(repository);
    }

    public Flowable<String> updateSession(String id, Flowable<Session> session) {
        return this.repository.updateSession(id, session);
    }

    @Override
    public Flowable<ResponseID> getIdFrom(String table) {
        return null;
    }
}
