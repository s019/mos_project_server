package com.mosfh.server.Backend.Services.Location;

import com.mosfh.server.Backend.Services.AbstractService;
import com.mosfh.server.Backend.repository.LocationRepository;

public class LocationService extends AbstractService<LocationRepository> {

    public LocationService(LocationRepository repository) {
        super(repository);
    }
}
