package com.mosfh.server.Backend.Services;

import com.mosfh.server.Backend.repository.AbstractRepository;
import com.mosfh.server.Backend.restapi.JSON.Objects.ResponseID;
import io.reactivex.Flowable;

public class AbstractService<T extends AbstractRepository> {

    protected T repository;

    public AbstractService(T repository) {
        this.repository = repository;
    }

    public Flowable<ResponseID> getIdFrom(String table) {
        return this.repository.getIdFrom(table);
    }
}
