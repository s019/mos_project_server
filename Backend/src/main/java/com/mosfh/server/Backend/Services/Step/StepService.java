package com.mosfh.server.Backend.Services.Step;

import com.mosfh.server.Backend.domain.Step;
import com.mosfh.server.Backend.repository.StepRepository;
import com.mosfh.server.Backend.restapi.JSON.Objects.Location;
import com.mosfh.server.Backend.restapi.JSON.Objects.ResponseID;
import io.reactivex.Flowable;
import org.springframework.stereotype.Service;

@Service
public class StepService implements IStepHandler {

    private StepRepository repository;

    public StepService(StepRepository repository) {
        this.repository = repository;
    }

    @Override
    public Flowable<Step> getSteps() {
        return this.repository.getSteps();
    }


    @Override
    public Flowable<ResponseID> createIdFor(Flowable<String> content) {
        return this.repository.createStepFrom(content);
    }

    @Override
    public Flowable<String> createLocationFor(Flowable<Location> location, String stepID) {
        return this.repository.createLocationFrom(location, stepID);
    }

    @Override
    public Flowable<ResponseID> createIdFor(Flowable<String> content, Flowable<String> sessionID) {
        return this.repository.createIdFor(content, sessionID);
    }

}

