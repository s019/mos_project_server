package com.mosfh.server.Backend.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.Date;

public final class Step {

    @JsonSerialize(using = ToStringSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING)

    private Date date;


    private String id;

    public Step() {
        this.date = null;
        this.id   = null;
    }

    public Step(String id, Date date) {
        this.id   = id;
        this.date = date;
    }

    public Step(String id) {
        this.id = id;
    }

    public Step(Date date) {
        this.date = date;
    }


    /** Getter and Setter Methods **/
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    /** toString-Override **/
    public String toString() {
        return "id: " + id + ", date: " + date.toString();
    }


    /** Builder Pattern Implementation **/
    public static class Builder {
        private String id;
        private Date date;

        public Builder() {
            this.id = null;
            this.date = null;
        }

        public Builder with(String id) {
            this.id = id;
            return this;
        }

        public Builder with(Date date) {
            this.date = date;
            return this;
        }

        public Step build() {
            return new Step(this.id, this.date);
        }
    }
}
