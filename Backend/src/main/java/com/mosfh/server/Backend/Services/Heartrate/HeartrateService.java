package com.mosfh.server.Backend.Services.Heartrate;

import com.mosfh.server.Backend.Services.AbstractService;
import com.mosfh.server.Backend.repository.HeartrateRepository;
import com.mosfh.server.Backend.restapi.JSON.Objects.Heartrate;
import io.reactivex.Flowable;
import org.springframework.stereotype.Service;

@Service
public class HeartrateService extends AbstractService<HeartrateRepository> implements IHeartrateHandler {

    public HeartrateService(HeartrateRepository repository) {
        super(repository);
    }

    public Flowable<String> updateHeartrate(String id, Flowable<Heartrate> heartrate) {
        return this.repository.updateHeartrate(id, heartrate);
    }
}
