package com.mosfh.server.Backend.Services.Step;

import com.mosfh.server.Backend.domain.Step;
import com.mosfh.server.Backend.restapi.JSON.Objects.Location;
import com.mosfh.server.Backend.restapi.JSON.Objects.ResponseID;
import io.reactivex.Flowable;

public interface IStepHandler {

    Flowable<Step> getSteps();
    Flowable<ResponseID> createIdFor(Flowable<String> content);
    Flowable<String> createLocationFor(Flowable<Location> location, String stepID);
    Flowable<ResponseID> createIdFor(Flowable<String> content, Flowable<String> sessionID);
}
