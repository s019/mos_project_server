package com.mosfh.server.Backend.restapi.JSON.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class Location extends AbstractObject {

    @NotNull
    @JsonProperty("y")
    private Double y;

    @NotNull
    @JsonProperty("x")
    private Double x;

    @NotNull
    @JsonProperty("StepID")
    private String stepID;

    public Location() {
        super();
    }

    public Location(String id) {
        super(id);
    }

    public Location(Double x, Double y, String id, String stepID) {
        super(id);
        this.y = y;
        this.x = x;
        this.stepID = stepID;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public String getStepID() {
        return stepID;
    }

    public void setStepID(String stepID) {
        this.stepID = stepID;
    }
}
