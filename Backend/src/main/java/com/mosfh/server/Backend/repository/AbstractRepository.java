package com.mosfh.server.Backend.repository;

import com.mosfh.server.Backend.Services.IAbstractHandler;
import com.mosfh.server.Backend.restapi.JSON.Objects.ResponseID;
import io.reactivex.Flowable;
import org.davidmoten.rx.jdbc.Database;

import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class AbstractRepository implements IAbstractHandler {

    protected final Database database;
    protected final String DATABASE_URL = "jdbc:mysql://localhost:3306/mos?user=root&password=";

    public AbstractRepository() throws SQLException, ClassNotFoundException {
            Class.forName("com.mysql.jdbc.Driver");
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            this.database = Database.from(this.DATABASE_URL, 20);
    }

    public Flowable<ResponseID> getIdFrom(String table) {

        Flowable<Integer> update = this
                .database
                .update("insert into " + table + "(id) value(null)")
                //.parameter(table)
                .counts();

        return this
                .database
                .select("select @last_" + table + "_uuid")
                //.parameter(table)
                .dependsOn(update)
                .getAs(String.class)
                .map(rawContent -> {
                    System.out.println(rawContent);
                    return new ResponseID(rawContent);
                });
    }
}
