package com.mosfh.server.Backend.Services;

import com.mosfh.server.Backend.restapi.JSON.Objects.ResponseID;
import io.reactivex.Flowable;

public interface IAbstractHandler { //<T> {

    Flowable<ResponseID> getIdFrom(String table);
    //Flowable<T> getAll();
}
