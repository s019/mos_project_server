package com.mosfh.server.Backend.repository;

import com.mosfh.server.Backend.restapi.JSON.Objects.Session;
import io.reactivex.Flowable;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Repository
public class SessionRepository extends AbstractRepository {

    public SessionRepository() throws SQLException, ClassNotFoundException {
        super();
    }

    public Flowable<String> updateSession(String id, Flowable<Session> session) {


        Flowable<Session> replaySession = session.replay().autoConnect();

        Flowable<Integer> update = this
                .database.update("Update session set trimp = ? where id = \"?\";")
                .parameterStream(replaySession.map(sess -> sess.getTrimp()))
                .counts();

        Flowable<Integer> update2 = this.database.update("update session set performance = ? where id = \"?\";")
                .parameterStream(replaySession.map(sess -> sess.getPerformance()))
                .dependsOn(update).counts();

        Flowable<Integer> update3 = this.database.update("update session set end_date = ? where id = \"?\";")
                .parameterStream(replaySession
                        .map(sess -> {
                            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(sess.getEnd());
                            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
                        }))
                .dependsOn(update2).counts();

        Flowable<Integer> update4 = this.database.update("update session set start_date = ? where id = \"?\";")
                .parameterStream(replaySession
                        .map(sess -> {
                            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(sess.getStart());
                            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
                        })).dependsOn(update3).counts();

        Flowable<Integer> update5 = this.database.update("update session set kcal = ? where id = \"?\";")
                .parameterStream(replaySession.map(sess -> sess.getKcal()))
                .dependsOn(update4).counts();

        Flowable<Integer> update6 = this.database.update("update session set fatigue = ? where id = \"?\";")
                .parameterStream(replaySession.map(sess -> sess.getFatigue()))
                .dependsOn(update5)
                .counts();

        Flowable<Integer> update7 = this.database.update("update session set fitness = ? where id = \"?\";")
                .parameterStream(replaySession.map(sess -> sess.getFitness()))
                .dependsOn(update5)
                .counts();

        return this
                .database
                .select("select id from session where id = \"" + id + "\";")
                .dependsOn(update7)
                .getAs(String.class);
    }
}
