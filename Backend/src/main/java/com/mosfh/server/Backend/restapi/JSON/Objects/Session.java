package com.mosfh.server.Backend.restapi.JSON.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Session {

    @JsonProperty
    private String id;

    @JsonProperty
    private Double trimp;

    @JsonProperty
    private Double performance;

    @JsonProperty
    private Double kcal;

    @JsonProperty
    private Double fitness;

    @JsonProperty
    private Double fatigue;

    @JsonProperty
    private String end;

    @JsonProperty
    private String start;

    public Session() { }
    public Session(String id, Double trimp, Double performance, Double kcal, Double fitness, Double fatigue, String end, String start) {
        this.id = id;
        this.trimp = trimp;
        this.performance = performance;
        this.kcal = kcal;
        this.fitness = fitness;
        this.fatigue = fatigue;
        this.end = end;
        this.start = start;
    }

    public String getId() {
        return id;
    }

    public Double getTrimp() {
        return trimp;
    }

    public Double getPerformance() {
        return performance;
    }

    public Double getKcal() {
        return kcal;
    }

    public Double getFitness() {
        return fitness;
    }

    public Double getFatigue() {
        return fatigue;
    }

    public String getEnd() {
        return end;
    }

    public String getStart() {
        return start;
    }
}
