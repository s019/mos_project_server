package com.mosfh.server.Backend.Services.Session;

import com.mosfh.server.Backend.Services.AbstractService;
import com.mosfh.server.Backend.repository.SessionRepository;
import com.mosfh.server.Backend.restapi.JSON.Objects.Session;
import io.reactivex.Flowable;
import org.springframework.stereotype.Service;

@Service
public class SessionService extends AbstractService<SessionRepository> implements ISessionHandler {

    public SessionService(SessionRepository repository) {
        super(repository);
    }

    public Flowable<String> updateSession(String id, Flowable<Session> session) {
        return this.repository.updateSession(id, session);
    }
}
