package com.mosfh.server.Backend.restapi;

import com.mosfh.server.Backend.Services.Session.SessionService;
import com.mosfh.server.Backend.restapi.JSON.Objects.ResponseID;
import com.mosfh.server.Backend.restapi.JSON.Objects.Session;
import io.reactivex.Flowable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping(path = "/sessions", produces = { APPLICATION_JSON_UTF8_VALUE })
public class SessionController extends AbstractController<SessionService> {

    public SessionController(SessionService sessionService) {

        super(sessionService, "session");
    }

    @PostMapping(path = "/{id}", produces = { APPLICATION_JSON_UTF8_VALUE })
    public Flowable<String> updateSession(@PathVariable String id, @RequestBody Flowable<Session> session) {
        return this.service.updateSession(id, session);
    }

    @RequestMapping(path = "/id", produces = { APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Flowable<ResponseID>> getId() {
        return new ResponseEntity<>(
                this.service.getIdFrom("session"),
                HttpStatus.CREATED
        );
    }
}
